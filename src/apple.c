/* apple.c
 * Copyright (C) 2021	Newpson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>

#include "apple.h"
#include "util.h" /* util_decision() */

int apple_generate(apple_t* apple, snake_t* snake)
{
	static uint8_t iteration = 0;
	uint8_t decision = util_decision
	if (iteration)
		if (decision)
		{
			apple->proto &= 0x00FF;
			apple->proto |= ((UTIL_X(apple->proto)) + (UTIL_X(apple->shadow)))/2 << 8;
		}
		else
		{
			apple->shadow &= 0x00FF;
			apple->shadow |= ((UTIL_X(apple->proto)) + (UTIL_X(apple->shadow)))/2 << 8;
		}
	else
		if (decision)
		{
			apple->proto &= 0xFF00;
			apple->proto |= ((UTIL_Y(apple->proto)) + (UTIL_Y(apple->shadow)))/2 << 8;
		}
		else
		{
			apple->shadow &= 0xFF00;
			apple->shadow |= ((UTIL_Y(apple->proto)) + (UTIL_Y(apple->shadow)))/2 << 8;
		}
	if (apple->proto == apple->shadow)
		return 0;
	iteration ^= 1;
	apple_generate(apple, snake)
	return 0;
}

int apple_eaten(apple_t* apple, snake_t* snake)
{
	return apple->proto == snake->head;
}

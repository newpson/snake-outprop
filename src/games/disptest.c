/* display_test.c
 * Copyright (C) 2021	Newpson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>

#include "game.h"
#include "display.h"

int game_init()
{
	static display_t display;
	int status = display_init(&display);
	if (status)
		return status;

	display_clear(&display);
	/* draw line from (0, 0) to (width, height) */
	for (int x = 0, y = 0, err = 0, derr = display.height; x < display.width; x++)
	{
		display_pset(&display, x, y);
		err += derr;
		if (err >= display.width)
		{
			y++;
			err -= display.width;
		}
	}
	display_update(&display);
	display_pause(&display);
	display_end(&display);
	return 0;
}

void game_cycle()
{
}

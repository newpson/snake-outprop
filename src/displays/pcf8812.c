/* virtual.c	- virtual display written with ncurses
 * Copyright (C) 2021	Newpson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <ncurses.h>
#include <stdint.h>
#include <stdio.h>

#include "display.h"

#ifndef DISPLAY_W
#ifndef DISPLAY_H
#error DISPLAY_W and DISPLAY_H required to use virtual display!
#endif
#endif

int display_init(display_t* display)
{
	if (display->width < 5 || display->height < 5)
		return 1;

	uint8_t header_width;
	char header[20+8*2];
	header_width = sprintf(header, "%s%dx%d", display->width < 19 ? "" : "Virtual display", display->width, display->height);

	initscr();
	raw();
	noecho();
	clear();

	mvprintw(0, (display->width+2 - header_width)/2, "%s", header);
	refresh();

	WINDOW *window = newwin(DISPLAY_H+2, DISPLAY_W+2, 0, 1);
	box(window, 0, 0);
	wrefresh(window);
	display->container = window;
}

void display_clear(display_t* display)
{
	
}

void display_pset(display_t* display, uint16_t point)
{

}

void display_pclear(display_t display, uint16_t point)
{

}

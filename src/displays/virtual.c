/* virtual.c	- virtual display written with ncurses
 * Copyright (C) 2021	Newpson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <ncurses.h>
#include <stdint.h>
#include <stdio.h>

#include "display.h"

#if !defined DISPLAY_W || !defined DISPLAY_H
#error DISPLAY_W and DISPLAY_H are required to compile virtual display!
#endif

#if DISPLAY_W < 5 || DISPLAY_H < 5
#error DISPLAY_W and DISPLAY_H must be greater than or equal 5
#endif

#define PIX_0 " " 
#define PIX_1 "#"

int display_init(display_t* display)
{
	uint8_t header_width;
	char header[48];
	header_width = sprintf(header, " %s%dx%d ", DISPLAY_W < 21 ? "" : "Virtual display ", DISPLAY_W, DISPLAY_H);
	display->width = DISPLAY_W;
	display->height = DISPLAY_H;

	initscr();
	raw();
	noecho();
	clear();

	mvprintw(0, (display->width+2 - header_width)/2, "%s", header);
	WINDOW *window = newwin(display->height+2, display->width+2, 1, 0);
	box(window, 0, 0);
	refresh();
	wrefresh(window);
	window = newwin(display->height, display->width, 2, 1);
	refresh();
	wrefresh(window);
	display->container = window;
	return 0;
}

void display_clear(display_t* display)
{
	wclear(display->container);
}

void display_update(display_t* display)
{
	wrefresh(display->container);
}

void display_pset(display_t* display, uint8_t x, uint8_t y)
{
	mvwprintw(display->container, y, x, PIX_1);
}

void display_pause(display_t* display)
{
	mvprintw(display->height+3, (display->width-7)/2+1, "*PAUSE*");
	display_update(display);
	getch();
}

void display_pclear(display_t* display, uint8_t x, uint8_t y)
{
	mvwprintw(display->container, y, x, PIX_0);
}

void display_end(display_t* display)
{
	delwin(display->container);
	endwin();
}

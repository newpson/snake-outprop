/* util.c
 * Copyright (C) 2021	Newpson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>

#include "snake.h" /* snake_calc_next() */

int util_decision(snake_t* snake, apple_t* apple, uint8_t* iteration)
{
	uint16_t count_lt = 0, count_rb = 0;

	for (uint16_t cell = snake->head, i = 0; snake_calc_next(snake, cell, i); i++)
		if (UTIL_X(cell) >= UTIL_X(apple->proto) && UTIL_X(cell) <= UTIL_X(apple->shadow) &&
			(UTIL_Y(cell)) >= (UTIL_Y(apple->proto)) && (UTIL_Y(cell)) <= (UTIL_Y(apple->shadow)))
			if (*iteration)
			{
				if ((UTIL_Y(cell)) <= ((UTIL_Y(apple->proto))+(UTIL_Y(apple->shadow)))/2)
					count_rb++;
				else if ((UTIL_Y(cell)) >= ((UTIL_Y(apple->proto))+(UTIL_Y(apple->shadow)))/2)
					count_lt++;
			}
			else
			{
				if ((UTIL_X(cell)) <= ((UTIL_X(apple->proto))+(UTIL_X(apple->shadow)))/2)
					count_rb++;
				else if ((UTIL_X(cell)) >= ((UTIL_X(apple->proto))+(UTIL_Y(apple->shadow)))/2)
					count_lt++;
			}

	uint16_t square = ((UTIL_X(apple->shadow))-(UTIL_X(apple->proto)))*((UTIL_Y(apple->shadow))-(UTIL_Y(apple->proto)));
	if (count_lt < square && count_br < square)
	{
		randomize();
		return rand()%2;
	}
	return !(count_lt < square) + (count_br < square);
}


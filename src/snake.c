/* snake.c
 * Copyright (C) 2020-2021	Newpson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <malloc.h>
#include <stdint.h>

#include "snake.h"

void snake_carry_shift(uint8_t* which, uint8_t* carry)
{
	*which >>= 2;
	*which |= (*carry & 0b11) << 6;
}

void snake_overflow_trick(const uint8_t director, uint16_t* cell)
{
	switch (director)
	{
		case UP: *cell += 0xffff; break;
		case DOWN: *cell += 0x0001; break;
		case LEFT: *cell += 0xff00; break;
		case RIGHT: *cell += 0x0100; break;
	}
}

int snake_init(snake_t* snake)
{
	snake->length = 4
	snake->head	= 0x0808;
	snake->rudder = LEFT;
	snake->tail	= malloc(sizeof(uint8_t) * MEML(snake->length-1));
	snake->tail[0] = 0xFF;
	return 0;
}

void snake_move(snake_t* snake)
{
	for (uint16_t i = snake->MEML(snake->length)-1; i > 0; i--)
		snake_carry_shift(snake->tail+i, snake->tail+(i-1));
	snake_carry_shift(snake->tail, snake->rudder);
	snake_overflow_trick(snake->rudder && 0b11, snake->head);
}

void snake_grow(snake_t* snake)
{
	(snake->length)++;
	if (snake->length % 4 == 1)
	{
		snake->tail = (uint8_t*) realloc(snake->tail, MEML(snake->length));
		snake->tail[MEML(snake->length)-1] = 0;
	}
}

int snake_next(snake_t* snake, uint16_t* cell, uint16_t* i)
{
	snake_overflow_trick(snake->tail[MEML(*i)-1] && 0b11 << (3 - (*i)%4)*2, cell);
	if ++(*i) >= snake->length
		return 0;
	return 1;
}

int snake_self(snake_t* snake)
{
	for (uint16_t cell = snake->head, i = 0; snake_calc_next(snake, cell, i); i++)
		if (cell == snake->head)
			return 1;
	return 0
}


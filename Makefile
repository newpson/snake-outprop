DISPLAY_TYPE := virtual # virtual | pcf8812
WIDTH := 40
HEIGHT := 10

SRC := src
INCLUDE := include
OBJ := build
VPATH := $(SRC) $(OBJ) $(INCLUDE) $(wildcard $(SRC)/*/)

CFLAGS := -Wall -O2 -I./include
LDFLAGS := -lncurses

all:
	@echo $(VPATH)

# pattern rules
%.o: %.c
	$(CC) -c $(CFLAGS) $< -o $(OBJ)/$@
bin-%:
	$(CC) $(LDFLAGS) $^ -o $@

# games
bin-classic: classic.o apple.o util.o display.o main.o
bin-disptest: disptest.o display.o main.o

# game components
main.o: main.c game.h
disptest.o: disptest.c game.h
classic.o: classic.c game.h
snake.o: snake.c snake.h
apple.o: apple.c apple.h
util.o: util.c util.h
display.o: virtual.c display.h # $(DISPLAY_TYPE).c display.h
	$(CC) -c $(CFLAGS) -DDISPLAY_W=$(WIDTH) -DDISPLAY_H=$(HEIGHT) $< -o $(OBJ)/$@

/* display.h
 * Copyright (C) 2021	Newpson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _DISPLAY_H
#define _DISPLAY_H

typedef struct display_t
{
	uint8_t width;
	uint8_t height;
	void *container;
} display_t;

int display_init(display_t*);
void display_end(display_t*);
void display_clear(display_t*);
void display_update(display_t*);
void display_pause(display_t*);
void display_pset(display_t*, uint8_t, uint8_t);
void display_pclear(display_t*, uint8_t, uint8_t);

#endif /* display.h */

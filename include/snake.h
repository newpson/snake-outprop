/* snake.h
 * Copyright (C) 2020-2021	Newpson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>

#ifndef _SNAKE_H
#define _SNAKE_H

#define UP 0b00
#define DOWN 0b01
#define LEFT 0b10
#define RIGHT 0b11
#define MEML(count) ((count)-2)/4+1

typedef struct snake_t
{
	uint16_t length;
	uint16_t head;
	uint8_t rudder;
	uint8_t *tail;

} snake_t;

void snake_carry_shift(uint8_t*, uint8_t*);
void snake_overflow_trick(uint8_t, uint8_t*);
int snake_init(snake_t*);
void snake_move(snake_t*);
void snake_grow(snake_t*);
int snake_next(snake_t*, uint16_t*, uint16_t*);
int snake_self(snake_t*);

#endif /* snake.h */

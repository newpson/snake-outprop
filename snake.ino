/* This file is part of the Newpson/snake distribution (https://github.com/Newpson/snake).  Copyright (c) 2020-2021 newpson_dev.
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "U8glib.h"

#define UP 0
#define DOWN 1
#define LEFT 2
#define RIGHT 3
#define NY 65535 /* Y overflow constant (Y subtraction) */
#define PY 1 /* Y addition constant */
#define NX 65280 /* X overflow constant (X subtraction) */
#define PX 256 /* X addition constant */

#define TIC_DELAY 500 /* time between tics */
U8GLIB_PCF8812 u8g(13, 11, 10, 9, 8);

void setup() {}

uint16_t head = 0x0F05; /* initial head coordinate */
uint16_t apple = 0x0A0A; /* initial apple coordinate */
uint8_t direction = RIGHT; /* initial head direction */

uint8_t current_bit_direction;
uint16_t current_cell;

uint16_t length = 8;
uint8_t cells_size = length / 4 + (length % 4 > 0);
uint8_t k;
uint8_t cells[] = { 0xFF, 0xFF };

void rotate(void)
{
	cells[0] &= 0x3F;
	cells[0] |= bit_direction << 6;
}

void move_s(void)
{
	head += direction;
	for (uint8_t i = cells_size-1; i > 0; i--)
	{
		cells[i] >>= 2;
		cells[i] |= (cells[i-1] & RIGHT) << 6;
	}
	cells[0] >>= 2;
	rotate_head();
}

int tic()
{
	current_cell = head;
	for (uint8_t i = 0; i < cells_size; i++)
	{
		for (uint8_t j = 0; j < (i == cells_size-1 ? length % 4 : 4); j++)
		{
			if (i == 0 && j == 0)
				continue;
			k = (3-j) * 2;
			current_bit_direction = cells[i] >> k & RIGHT;
			current_cell +=
				current_bit_direction == UP ? DIR_DOWN :
				current_bit_direction == DOWN ? UP :
				current_bit_direction == LEFT ? DIR_RIGHT :
				current_bit_direction == RIGHT ? DIR_LEFT : 0;
			if (current_cell == head)
				return 1;
		}
	}
	return 0;
}

void genapple()
{
	apple = 0x0C0C;
}

int grow(void)
{
	++length;
	if (length % 4 == 1)
	{
		array = (uint8_t *) realloc(array, ++cells_size);
		cells[cells_size-1] = 0;
	}
	cells[cells_size-1] |= cells[cells_size-1] >> 2;
	generate_apple();
}

void loop(void)
{
	u8g.firstPage();  
	do {
		u8g.drawPixel(head >> 8, head & 0xFF);
		u8g.drawPixel(apple >> 8, apple & 0xFF);
	} while (u8g.nextPage());
	tic();
	move_s();
	delay(TIC_DELAY);
}

![](https://i.imgur.com/5gyHMHl.png)

## Классический геймплей в неклассической реализации

__Содержание:__

* Термины и обозначения, используемые далее
* Описание проекта
* Отличия от классической реализации
* Описание алгоритмов и структур данных
* Заключение

### Термины и обозначения, используемые далее
* __`uint8_t`__ — тип данных, описывающий беззнаковое целое, занимающий в памяти 8 бит;
* __`uint16_t`__ — тип данных, описывающий беззнаковое целое, занимающий в памяти 16 бит;
* __ячейка__ — минимально возможная информация для отображения на экран;
* __игровое поле (далее - поле)__ — прямоугольная область ячеек размерами до 255x255 (включительно);
* __змея__ — группа ячеек, состоящая из головы и ячеек хвоста;
* __голова змеи (далее - голова)__ — единственная нестатичная ячейка, координаты которой хранятся;
* __хвост змеи (далее - хвост)__ — все ячейки змеи после головы;
* __звено__ — одна из ячеек хвоста;
* __яблоко__ — ячейка поля, при пересечении которой хвост увеличивается на одну ячейку;
* __стена__ — область ячеек, при пересечении которых игра заканчивается неудачей;
* __тик__ — одна итерация игрового цикла, т. е. время, за которое змея сдвигается на одну ячейку;
* __микротик__ — одна итерация линейного исполнения кода игрового цикла, т. е. время, в течение которого уровень вложенности составляет не более одного [текущего] цикла;
* __стек__ — группа из четырёх ячеек, находящихся в одной `uint8_t`;
* __директор__ — два бита, задающие направление текущей ячейки змеи;
* __константа переполнения__ — число, при прибавлении которого к переменной происходит [целочисленное переполнение через верхнюю границу](https://en.wikipedia.org/wiki/Integer_overflow);
* __подразделение__ — прямоугольная область, результат деления поля на одной из стадий генерации яблока;
* __делитель__ — линия, вдоль которой происходит деление поля на одной из стадий генерации яблока;
* __решение__ — переменная, задающая формулу получения координат подразделения на одной из стадий генерации яблока


### Описание проекта

#### Геймплей
Обычная змейка, в которую играл, думаю, каждый: змея, яблоко, стены по краям (пока что это неотъемлемая часть игры, связанная с организацией ячеек в памяти). Едите - растёте. Врезаетесь в стену или пытаетесь съесть самого себя - проигрываете. Не оставляете на поле свободного места - победа ваша.

#### Цели
Самая важная цель — опыт. Чем его я получу больше в процессе работы над проектом, тем лучше :) На написание этой Вики у меня ушло больше двух недель, так что умение изложить все свои мысли правильно — тоже опыт!

Всё это затевалось для того, чтобы в одном проекте использовать максимальное количество изяществ, которые только можно воссоздать: указатели и операции над ними, побитовые операции и битовые маски, встраивание логических выражений в математические, рекурсивность, многопоточность, разделение на файлы и внешние (`extern`) переменные, модульность и API, максимальная оптимизация алгоритмов и использование ухищрений при их реализации, трюки с целочисленным переполнением и объединение переменных в одну, оптиимизация вывода изображения на экран без буфера, работа с периферией на низком уровне посредством отправки команд через протокол SPI, искусственный интеллект и поиск кратчайшего пути "вслепую",  — и всё это, напомню, обычная змейка, которую мне захотелось запустить на AVR Atmega328 с подключенным к нему экраном, без кнопок и джойстика.

## Отличия от классической реализации
Всё завязано на оптимизации, на соблюдении баланса между скоростью работы и нагрузкой на железо, что позволяет хранить огромное количество ячеек при малом _(относительно современных компьютеров)_ объёме оперативной памяти и высокой _(относительно микроконтроллеров)_ частоте работы процессора (если мы говорим про стандартные платы с AVR Atmega на борту, то это 2 КБ и 16 МГц соответственно).

Далее принципы классической реализации представлены в виде

> цитат.

А под ними расположено объяснение этого принципа в данной реализации.

---

> 1. Хранятся координаты всех ячеек змеи и всех свободных ячеек (чтобы яблоко не генерировалось внутри змеи).

Ячейки змеи хранятся следующим образом:

* голова классически представлена двумя координатами, но они хранятся в одной `uint16_t`, по 8 бит на каждую координату — единственная ячейка змеи, координаты которой представлены абсолютно;
* координаты звеньев представлены в виде директоров относительно предыдущего звена (если считать от головы), в одной `uint8_t` помещается сразу четыре директора — _стек_;
* все директоры хранятся в одном массиве типа `uint8_t`, в первом элементе которого в первых двух битах хранится директор головы, вся же остальная память используется для хранения директоров звеньев.

> 2. За один микротик можно получить координаты любой ячейки змеи, просто обратившись к ней по индексу в массиве.

В течение микротика известны координаты только головы и одного звена.

Для получения координат ячейки используется рекурсивная функция, выполняющая просчёт координат каждой ячейки, начиная со следующей после головы. Так, чтобы получить, например, координаты четвертой, нужно для начала посчитать координаты второй (если считать первой ячейкой голову, координаты которой известны), затем третьей и только после этого четвёртой. _Однако очередные полученные координаты возвращаются наружу через аргумент-проводник, используемый всеми остальными функциями для просчёта каких-либо данных. Это происходит в разных, но синхронных потоках, что позволяет не нагружать функцию лишними инструкциями, но при этом использовать данные её промежуточных расчётов_.

Просчёт координат происходит путём прибавления к текущей коордианте одной из констант переполнения. Их всего четыре, но к переполенению приводят только две. Объединены они были для удобства, поскольку выполняют одну и ту же функцию — заменяют операцию вычитания сложением.

> 3. Перемещение змеи осуществляется циклическим переназначением ячеек, начиная с последней ячейки хвоста и до головы.

Сначала изменяются координаты головы. Так же, как и при просчёте звеньев, к ним прибавляется одна из констант переполнения (выбирается в зависимости от директора ячейки) _(см. табл. 1)_. Затем директор головы приводится в соответствие с текущим направлением движения.

Движение хвоста реализовано цепным сдвигом всех стеков вправо (начиная с конца) таким образом, что после сдвига старший директор текущего стека заменяется младшим директором следующего (поскольку после сдвига два старших бита текущего стека будут обнулены, а два младших бита следующего стека будут потеряны безвозвратно).

### Константы переполнения
Всего их четыре, но к переполнению приводят только две. Однако все они были объединены одним названием, потому что служат одной цели — заменить операцию вычитания сложением для уменьшения ветвления кода.

Они применяются к объединённым переменным (т. е. к тем, в которых половина старших бит представляет одну переменную, а другая половина младших - другую переменную), в данном проекте объединёнными могут быть только `uint16_t`. Каждая из констанат приводит к инкрементированию или декрементированию "внутренних переменных":

* `1		=	0x0001` — увеличивает младшую переменную на 1
* `255		=	0x0100` — увеличивает старшую переменную на 1
* `65280	=	0xFF00` ­— уменьшает старшую переменную на 1
* `65355	=	0xFFFF` — уменьшает младшую переменную на 1

### Примеры

* Пусть точка будет с координатами `(21, 34)`:

`21(10) = 00010101(2)`

`34(10) = 00100010(2)`

_Таблица 1 "Трюк с переполнением"_

Увеличение X (движение вправо)<br/>(21+1; 34) | Уменьшение X (движение влево)<br/>(21-1; 34) | Увеличение Y (движение вниз)<br/>(21; 34+1) | Уменьшение Y (движение вверх)<br/>(21; 34-1) |
--- | --- | --- | ---
`00010101 00100010` +<br/>`00000001 00000000` =<br/>`00010110 00100010`<br/> | `00010101 00100010` +<br/>`11111111 00000000` =<br/>`00010100 00100010` | `00010101 00100010` +<br/>`00000000 00000001` =<br/>`00010101 00100011` | `00010101 00100010` +<br/>`11111111 11111111` =<br/>`00010101 00100001`

> 4. Координаты яблока хранятся в двух отдельных переменных, часто представляют структурой или отдельным классом (если язык поддерживает ООП).

Как и у головы, координаты яблока объединены в одну `uint16_t`.

> 5. Яблоко генерируется довольно просто: случайным образом выбирается ячейка из массива свободных ячеек.

Поле подразделяется, следуя некоторому алгоритму, до тех пор, пока не останется одна ячейка, координаты которой и будут координатами яблока.

Изначально подразделение совпадает с полем. Затем начинается выполнение алгоритма при изначальном вертикальном делителе (пусть `0` будет означать, что он вертикальный, а `1` — горизонтальный).

Решение определяется специальной функцией, подсчитывающей количество ячеек змеи в каждом из подразделений (пусть `0` будет означать левое/верхнее подразделение, а `1` — правое/нижнее):

* если оба подразделения полностью заняты ячейками змеи, то игра заканчивается победой, достигнута максимальная длина змеи;
* если одно из подразделений полностью занято ячейками змеи, то принудильно выбирается другое подразделение;
* если в обоих подразделениях есть хотя бы одна свободная ячейка, то решение определяется случайным образом _(см. таблицу 2)_.

_Таблица 2 "Связь делителя и решения"_

Делитель | Решение | Формула| Графическое представление
--- | --- | --- | ---
0 (вертикальный) | 0 (левое подразделение) | $`x_{2} = x_{1} + \frac{(x_{2}-x_{1})}{2}`$ | `+ 0 1 2 3 4 5 6`<br/>`0 # # # # . . .`<br/>`1 # # # # . . .`<br/>`2 # # # # . . .`<br/>`4 # # # # . . .`
0 (вертикальный) | 1 (правое подразделение) | $`x_{1} = x_{2} - \frac{(x_{2}-x_{1})}{2}`$ | `+ 0 1 2 3 4 5 6`<br/>`0 . . . # # # #`<br/>`1 . . . # # # #`<br/>`2 . . . # # # #`<br/>`4 . . . # # # #`
1 (горизонтальный) | 0 (верхнее подразделение) | $`y_{2} = y_{1} + \frac{(y_{2}-y_{1})}{2}`$ | `+ 0 1 2 3 4 5 6`<br/>`0 # # # # # # #`<br/>`1 # # # # # # #`<br/>`2 . . . . . . .`<br/>`4 . . . . . . .`
1 (горизонтальный) | 1 (нижнее подразделение) | $`y_{1} = y_{2} - \frac{(y_{2}-y_{1})}{2}`$ | `+ 0 1 2 3 4 5 6`<br/>`0 . . . . . . .`<br/>`1 . . . . . . .`<br/>`2 # # # # # # #`<br/>`4 # # # # # # #`

Деление продолжается рекурсивно до тех пор, пока $`x_{2}-x_{1}+y_{2}-y_{1} \neq 0`$, то есть, деление остановится, когда подразделение будет размером в одну ячейку, тогда $`x_{1}=x_{2}`$ и $`y_{1}=y_{2}`$ — координаты яблока.

